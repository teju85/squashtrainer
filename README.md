 [![pipeline status](https://gitlab.com/teju85/squashtrainer/badges/master/pipeline.svg)](https://gitlab.com/teju85/squashtrainer/-/commits/master)
 [![coverage report](https://gitlab.com/teju85/squashtrainer/badges/master/coverage.svg)](https://gitlab.com/teju85/squashtrainer/-/commits/master)
 [![Latest Release](https://gitlab.com/teju85/squashtrainer/-/badges/release.svg)](https://gitlab.com/teju85/squashtrainer/-/releases)

# Introduction
This project hosts the source files for the website: https://teju85.gitlab.io/squashtrainer/

# Requirements
TBD

# Deployment
TBD
