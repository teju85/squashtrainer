
// Stringify the input time in MM:SS format
function formatTimeMMSS(time) {
    const minutes = Math.floor(time / 60);
    let seconds = time % 60;
    if (seconds < 10) {
        seconds = `0${seconds}`;
    }
    return `${minutes}:${seconds}`;
}

const MS_PER_S = 1000;

const NUM_RALLIES = 11;  // number of rallies per game

const ARROWS = [
    "&#8599;",  // front right
    "&#8594;",  // right
    "&#8600;",  // back right
    "&#8601;",  // back left
    "&#8592;",  // left
    "&#8598;"   // front left
];

const ARROW_NAMES = [
    "Front Right",
    "Right",
    "Back Right",
    "Back Left",
    "Left",
    "Front Left"
];

const FrontRight = 0;
const Right      = 1;
const BackRight  = 2;
const BackLeft   = 3;
const Left       = 4;
const FrontLeft  = 5;

class Event {
    constructor(durationInS, renderFunc) {
        this.durationInS = durationInS;
        this.renderFunc = renderFunc;
        this.elapsedInS = 0;
    }
    render() { this.renderFunc(this); }
    step() { this.elapsedInS += 1; }
    done() { return this.elapsedInS >= this.durationInS; }
    timeLeft() { return this.durationInS - this.elapsedInS; }
    timeLeftMMSS() { return formatTimeMMSS(this.timeLeft()); }
};  // class Event

class EventQueue {
    // clearFunc is called after the end of every event
    // initialFunc is called once before the setInterval call happens
    // finalFunc is called after clearInterval call happens
    constructor(clearFunc, initialFunc = null, finalFunc = null) {
        this.queue = [];
        this.totalDurationInS = 0;
	this.clearFunc = clearFunc;
        this.initialFunc = initialFunc;
        this.finalFunc = finalFunc;
    }
    add(durationInS, renderFunc) {
        this.queue.push(new Event(durationInS, renderFunc));
        this.totalDurationInS += durationInS;
    }
    // start the timer once you have pushed all the events
    start() {
	// just render the first event before timer starts
	if (this.queue.length <= 0) {
	    return;
	}
        if (this.initialFunc) {
            this.initialFunc();
        }
	this.queue[0].render();
	let ti = setInterval(() => {
	    if (this.render()) {
		clearInterval(ti);
                if (this.finalFunc) {
                    this.finalFunc();
                }
	    }
	}, MS_PER_S);
    }
    render() {
	if (this.queue.length == 0) {
	    return true;
	}
	this.queue[0].step();
        if (this.queue[0].done()) {
	    this.queue.splice(0, 1);
	    this.clearFunc();
	    if (this.queue.length == 0) {
		return true;
	    }
	}
	this.queue[0].render();
	return this.queue[0].done();
    }
};  // class EventQueue

function playSound(file) {
    var audio = new Audio(file);
    audio.loop = false;
    return audio.play();
}

// number of seconds before the end of the rest interval to start notifying the
// user about it (typically by playing a sound)
const REST_END_THRESHOLD = 5;
// number of seconds to wait before starting the timer. This gives the user to
// start the timer on their devices and go and get ready on the 'T' (for eg)
const START_DELAY_S = 15;

function showDelay(evt, soundFile, elem, message) {
    let left = evt.timeLeft();
    if (soundFile != null && left > 0 && left <= REST_END_THRESHOLD) {
        playSound(soundFile);
    }
    elem.innerHTML = message;
}

function addStartDelay(statusElem, eq, soundFile = null, delayInS = START_DELAY_S) {
    eq.add(START_DELAY_S, (evt) => {
        let str = evt.timeLeftMMSS();
        showDelay(evt, soundFile, statusElem, `Starting in ${str}`);
    });
}

function requestFullScreenEnter(elem) {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.webkitRequestFullscreen) { /* Safari */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE11 */
        elem.msRequestFullscreen();
    }
}

function requestFullScreenExit() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.webkitExitFullscreen) { /* Safari */
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { /* IE11 */
        document.msExitFullscreen();
    }
}

function randInt(start, end) {
    let diff = end - start;
    let num = Math.floor(Math.random() * diff);
    return num + start;
}

function flashAndPlaySoundAtStart(event, elem, file) {
    if (event.elapsedInS == 0) {
        elem.classList.add("flash");
        return playSound(file);
    } else {
        elem.classList.remove("flash");
    }
}
