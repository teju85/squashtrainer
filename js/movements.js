
const NEW_SHOT_FILE = "../assets/squash-ball-hit-sound.mp3"
const REST_END_FILE = "../assets/end-of-rest.mp3";

// add a "resting" event to the input event queue.
// Typically useful when one wants to add breaks between exercises
function addRest(statusElem, eq, resting, set, sets, soundFile = null) {
    eq.add(resting, (evt) => {
        let str = evt.timeLeftMMSS();
        showDelay(evt, soundFile, statusElem,
                  `Set: ${set}/${sets}<br>Breath and rest for ${str}`);
    });
}

function addExercise(eq, set, sets, id, directions) {
    let reps = parseInt(document.getElementById("reps").value);
    let orig_reps = reps;
    if (id != 0 && id != 1) {
        reps *= 2;
    }
    let timePerRep = parseInt(document.getElementById("timePerRep").value);
    for (let rep = reps; rep > 0; rep--) {
        eq.add(timePerRep, (evt) => {
            let str = evt.timeLeftMMSS();
            let arrowstr, arrow;
            let repid = reps - rep + 1;
            if (id == 0 || id == 1) {
                arrowstr = ARROW_NAMES[directions[id]];
                arrow = ARROWS[directions[id]];
            } else {
                arrowstr = ARROW_NAMES[directions[rep % 2]];
                arrow = ARROWS[directions[rep % 2]];
                repid = Math.ceil(repid / 2);
            }
            document.getElementById("status").innerHTML = `${str}<br>Set: ${set}/${sets} Rep: ${repid}/${orig_reps}<br>${arrowstr}`;
            let a = document.getElementById("arrows");
            a.innerHTML = `${arrow}`;
            flashAndPlaySoundAtStart(evt, a, NEW_SHOT_FILE);
        });
    }
}

function doMovementsExercise(directions) {
    let statusElem = document.getElementById("status");
    eq = new EventQueue(
        () => {
            statusElem.innerHTML = "";
            let a = document.getElementById("arrows");
            a.innerHTML = "";
            a.classList.remove("flash");
        },
        () => {
            requestFullScreenEnter(document.getElementById("timer"));
        },
        () => {
            requestFullScreenExit();
        });
    // starting delay
    addStartDelay(document.getElementById("status"), eq, REST_END_FILE);
    let sets = parseInt(document.getElementById("sets").value);
    let resting = parseInt(document.getElementById("resting").value);
    // each set
    for (let set = sets; set > 0; set--) {
	let rem = sets - set + 1
        // exercises
        addExercise(eq, rem, sets, 0, directions);
        addRest(statusElem, eq, resting, rem, sets, REST_END_FILE);
        addExercise(eq, rem, sets, 1, directions);
        addRest(statusElem, eq, resting, rem, sets, REST_END_FILE);
        addExercise(eq, rem, sets, 2, directions);
        // add rest for all but the last exercise of the last set
        if (set != 1) {
            addRest(statusElem, eq, resting, rem, sets, REST_END_FILE);
        }
    }
    eq.start();
}

function addRally(statusElem, eq, game, games, rally) {
    let reps = NUM_RALLIES;
    let timePerShot = parseInt(document.getElementById("timePerShot").value);
    let shots = parseInt(document.getElementById("shots").value);
    let id = 0;
    for (let shot = 1; shot <= shots; shot++) {
        eq.add(timePerShot, (evt) => {
            let str = evt.timeLeftMMSS();
            if (evt.elapsedInS == 0) {
                id = randInt(0, ARROW_NAMES.length);
            }
            let arrowstr = ARROW_NAMES[id];
            let arrow = ARROWS[id];
            document.getElementById("status").innerHTML = `${str}<br>Game: ${game}/${games} Rally: ${rally}/${NUM_RALLIES} Shot: ${shot}/${shots}<br>${arrowstr}`;
            let a = document.getElementById("arrows");
            a.innerHTML = `${arrow}`;
            flashAndPlaySoundAtStart(evt, a, NEW_SHOT_FILE);
        });
    }
    // add rest
    if (game != games || rally != NUM_RALLIES) {
        let resting = parseInt(document.getElementById("resting").value);
        eq.add(resting, (evt) => {
            let str = evt.timeLeftMMSS();
            showDelay(evt, REST_END_FILE, statusElem,
                      `Game: ${game}/${games} Rally: ${rally}/${NUM_RALLIES}<br>Breath and rest for ${str}`);
        });
    }
}

function doRandomMovementsExercise(directions) {
    let statusElem = document.getElementById("status");
    eq = new EventQueue(
        () => {
            statusElem.innerHTML = "";
            let a = document.getElementById("arrows");
            a.innerHTML = "";
            a.classList.remove("flash");
        },
        () => {
            requestFullScreenEnter(document.getElementById("timer"));
        },
        () => {
            requestFullScreenExit();
        });
    // starting delay
    addStartDelay(statusElem, eq, REST_END_FILE);
    let games = parseInt(document.getElementById("numGames").value);
    // each game
    for (let game = 1; game <= games; game++) {
        for (let rally = 1; rally <= NUM_RALLIES; rally++) {
            addRally(statusElem, eq, game, games, rally);
        }
    }
    eq.start();
}

function addSet(eq, set, sets, directions) {
    let reps = directions.length;
    let timePerRep = parseInt(document.getElementById("timePerRep").value);
    for (let rep = 0; rep < reps; rep++) {
        eq.add(timePerRep, (evt) => {
            let str = evt.timeLeftMMSS();
            let repid = rep + 1;
            let arrowstr = ARROW_NAMES[directions[rep]];
            let arrow = ARROWS[directions[rep]];
            document.getElementById("status").innerHTML = `${str}<br>Set: ${set}/${sets} Rep: ${repid}/${reps}<br>${arrowstr}`;
            let a = document.getElementById("arrows");
            a.innerHTML = `${arrow}`;
            flashAndPlaySoundAtStart(evt, a, NEW_SHOT_FILE);
        });
    }
}

function doAllMovementsExercise(directions) {
    let statusElem = document.getElementById("status");
    eq = new EventQueue(
        () => {
            statusElem.innerHTML = "";
            let a = document.getElementById("arrows");
            a.innerHTML = "";
            a.classList.remove("flash");
        },
        () => {
            requestFullScreenEnter(document.getElementById("timer"));
        },
        () => {
            requestFullScreenExit();
        });
    // starting delay
    addStartDelay(document.getElementById("status"), eq, REST_END_FILE);
    let sets = parseInt(document.getElementById("sets").value);
    let resting = parseInt(document.getElementById("resting").value);
    // each set
    for (let set = sets; set > 0; set--) {
	let rem = sets - set + 1
        addSet(eq, rem, sets, directions);
        // add rest for all but the last exercise of the last set
        if (set != 1) {
            addRest(statusElem, eq, resting, rem, sets, REST_END_FILE);
        }
    }
    eq.start();
}

function doDiagonalMovementsExercise(directions1, directions2) {
    let statusElem = document.getElementById("status");
    eq = new EventQueue(
        () => {
            statusElem.innerHTML = "";
            let a = document.getElementById("arrows");
            a.innerHTML = "";
            a.classList.remove("flash");
        },
        () => {
            requestFullScreenEnter(document.getElementById("timer"));
        },
        () => {
            requestFullScreenExit();
        });
    // starting delay
    addStartDelay(document.getElementById("status"), eq, REST_END_FILE);
    let sets = parseInt(document.getElementById("sets").value);
    let resting = parseInt(document.getElementById("resting").value);
    // each set
    for (let set = sets; set > 0; set--) {
	let rem = sets - set + 1
        addExercise(eq, rem, sets, 2, directions1);
        addRest(statusElem, eq, resting, rem, sets, REST_END_FILE);
        addExercise(eq, rem, sets, 2, directions2);
        // add rest for all but the last exercise of the last set
        if (set != 1) {
            addRest(statusElem, eq, resting, rem, sets, REST_END_FILE);
        }
    }
    eq.start();
}
